function bubbleTri() {
    let tab = [4, 2, 5, 6, 3, 8, 1, 7, 9];
    for (i = 0; i < tab.length; i++) {
        for (j = 0; j < i; j++) {
            if ( tab[j] > tab[j + 1]) {
                tmp = tab[j];
                tab[j] = tab[j + 1];
                tab[j + 1] = tmp;
            }
            if(tab[j] === 1){
                tab.unshift(tab.splice(j, 1)[0]);
            }
        }
    }
    console.log(tab)
}